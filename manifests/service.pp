# Class: nrpe::service
#
# This module manages the apache package.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class nrpe::service {
  
  service { 'nrpe':
    ensure => 'running',
    enable => true,
  }
    
}