# == Class: nrpe
#
# Full description of class nrpe here.
#
# === Parameters
#
# Document parameters here.
#
# [*cpus*]
# Number of CPUs on the machine.
#
# [*architecture*]
# 32bit or 64bit. Values to pass are 32 or 64.
# 
# [**]
#
# [**]
#
# === Examples
#
#  include nrpe
#
# === Authors
#
# Author Name <mtravis@webcourseworks.com>
#
# === Copyright
#
# Copyright 2014 Your name here, unless otherwise noted.
#
class nrpe (

){



  anchor { 'nrpe::begin': } ->
    class  { '::nrpe::package': } ->
    class  { '::nrpe::config': } ~>
    class  { '::nrpe::service': } ->
  anchor { 'nrpe::end': }

}
