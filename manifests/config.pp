# Class: nrpe::config
#
# This module manages the apache package.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class nrpe::config {
    
  file { '/etc/nagios/nrpe.cfg':
    ensure  => 'file',
    content => template('nrpe/nrpe.cfg.erb'),
    mode    => '0644',
    notify  => Service['nrpe']
  }
   
}