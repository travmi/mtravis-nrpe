# Class: nrpe::package
#
# This module manages the apache package.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class nrpe::package {
  
  package { 'nrpe':
    ensure => 'latest',
  }
  
  package { 'nagios-plugins-disk':
    ensure => 'latest',
  }
 
 package { 'nagios-plugins-load':
    ensure => 'latest',
  }
  
  package { 'nagios-plugins-mailq':
    ensure => 'latest',
  }
  
  package { 'nagios-plugins-ntp':
    ensure => 'latest',
  } 

  package { 'nagios-plugins-perl':
    ensure => 'latest',
  }
  
  package { 'nagios-plugins-procs':
    ensure => 'latest',
  }
  
  package { 'nagios-plugins-swap':
    ensure => 'latest',
  }

  package { 'nagios-plugins-users':
    ensure => 'latest',
  }

}